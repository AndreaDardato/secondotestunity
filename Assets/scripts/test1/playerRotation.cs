﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerRotation : MonoBehaviour
{

    public float sensitivity = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float rotation = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        //transform.Rotate(0, 0, rotation);
        transform.Rotate(Vector3.up * sensitivity * Time.deltaTime);


    }
}
