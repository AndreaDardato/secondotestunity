﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouseLook : MonoBehaviour
{

    float sensitibity = 100.0f;
    public Transform player;
    public Transform gun;

    float xRotation = 0f;



    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    // Update is called once per frame
    void Update()
    {

        float mouseX = Input.GetAxis("Mouse X") * sensitibity * Time.deltaTime; //da destra a sinistra
        float mouseY = Input.GetAxis("Mouse Y") * sensitibity * Time.deltaTime; // dall'alto al basso

        xRotation -= mouseY;

        xRotation = Mathf.Clamp(xRotation, -90, 90);    //può andare da -90 a 90 gradi

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);    //serve per far ruotare la cam (in alto)
        gun.localRotation = Quaternion.Euler(xRotation, 0, 0);  //ruoto anche la gun in alto

        player.Rotate(Vector3.up * mouseX);
        gun.Rotate(Vector3.up * mouseX);    //faccio andare da destra a sinistra la gun
        
    }
}
