﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletColliderScript : MonoBehaviour
{

    public float damagePerBullet = 20.0f;

    void Update()
    {
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.collider.tag);

        if(collision.collider.tag == "enemys")
        {
            //richiamo il componente (script) enemyScript
            collision.collider.GetComponent<enemyScript>().loseHP(damagePerBullet);

        }


    }

}
