﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    /*
     *  da aggiungere:
     *  Sistema di controllo delle collisioni
     */ 

    public float speed = 20f;
    public float lifeDuration = 3f; //vive per 3 secondi

    private float timer;

    // Start is called before the first frame update
    void Start()
    {

        timer = lifeDuration;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
        timer -= Time.deltaTime;
        if(timer <= 0f)
        {
            Destroy(gameObject);
        }
    }
}
