﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyScript : MonoBehaviour
{

    public float hpLife = 150f;

    public void loseHP(float ammount)
    {
        hpLife -= ammount;

        //Debug.Log("HP: " + hpLife);
        if (hpLife <= 0)
        {
            Debug.Log("Enemy killed");
            Destroy(gameObject);    //distruggi il gameObject che ha questo script
        }
    }


    void Update()
    {
        
    }
}
