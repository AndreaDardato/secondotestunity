﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{

    public CharacterController controller;
    public Camera playerCamera;

    public float speed = 10.0f;

    public float gravity = -9.81f;

    Vector3 velocity;    //serve per la velocità corrente
    bool isGrounded;

    public Transform groundChecker;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    public float jumpHeight = 3.0f;

    private int flag = 0;


    private void FOVController()
    {
        if (Input.GetKeyDown("n"))
        {
            playerCamera.fieldOfView = 90;
            flag++;
        }
        if (flag % 2 != 0) //se flag è dispari
        {
            playerCamera.fieldOfView = 60;
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {

        //controlliamo se c'è contatto con il terreno
        isGrounded = Physics.CheckSphere(groundChecker.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = 0;
        }

        //ci servono gli input
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");


        //dobbiamo prendere questi input e trasformarli in direzione
        Vector3 move = transform.right * x + transform.forward * z; //la prima parte serve per calcolare
                                                                    //il movimento sull'asse delle x (laterale)
                                                                    //mentre l'altra sull'asse delle z (in su)

        controller.Move(move * speed * Time.deltaTime);


        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }


        //per far si che il giocatore abbia gravità usiamo
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        FOVController();
        

    }




}
